from Design.Builder.damper import DamperBuilder
from Design.Builder.mass import MassBuilder
from Design.Builder.spring import SpringBuilder


class SpringMassDamperAssembler:
    def __init__(self):
        self.mass = MassBuilder()
        self.damper = DamperBuilder()
        self.spring = SpringBuilder()

    def initialize_default_values(self):
        self.mass.weight = 100
        self.spring.stiffness = 100
        self.damper.damping = 50