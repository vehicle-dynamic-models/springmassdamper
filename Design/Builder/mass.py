from ISaveLoad import ISaveLoad


class MassBuilder(ISaveLoad):
    def __init__(self, name="", weight_value=0):
        super().__init__(".wght", "0.0.0", name)
        self._weight = weight_value

    @property
    def weight(self):
        return self._weight

    @weight.setter
    def weight(self, value):
        if value < 0:
            raise ValueError("Value cannot be negative")
        elif value == 0:
            raise ValueError("Value cannot be zero")
        else:
            self._weight = value

    # ISaveLoad Implementation
    def init_default_values(self):
        pass

    def save_object(self, file_path=""):
        pass

    def load_object(self, file):
        pass
