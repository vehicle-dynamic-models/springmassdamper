from ISaveLoad import ISaveLoad


class SpringBuilder(ISaveLoad):
    def __init__(self, name="", spring_value=0):
        super().__init__("spr", "0.0.0", name)
        self._stiffness = spring_value

    @property
    def stiffness(self):
        return self._stiffness

    @stiffness.setter
    def stiffness(self, value):
        if value < 0:
            raise ValueError("Value cannot be negative")
        elif value == 0:
            raise ValueError("Value cannot be zero")
        else:
            self._stiffness = value

    # ISaveLoad Implementation
    def init_default_values(self):
        pass

    def save_object(self, file_path=""):
        pass

    def load_object(self, file):
        pass
