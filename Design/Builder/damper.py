from ISaveLoad import ISaveLoad


class DamperBuilder(ISaveLoad):
    def __init__(self, name="", damping_value=0):
        super().__init__(".dmp", "0.0.0", name)

        self._damping = damping_value

    @property
    def damping(self):
        return self._damping

    @damping.setter
    def damping(self, value):
        self._damping = value

# ISaveLoad Implementation
    def init_default_values(self):
        pass

    def save_object(self, file_path=""):
        pass

    def load_object(self, file):
        pass
