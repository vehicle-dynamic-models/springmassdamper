import numpy as np


class InputBuilder:
    def __init__(self):
        self._road_input = np.empty(0)

    @property
    def road_input(self):
        return self._road_input

    def create_road_input(self, initial_value, final_value, step_size):
        self._road_input = np.linspace(initial_value, final_value, step_size)