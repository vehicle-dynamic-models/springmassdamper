import numpy as np
from scipy.integrate import odeint
from Design.quarter import SpringMassDamperAssembler
from Results.smd_r import SpringMassDamper_R
from Design.Builder.inputs import InputBuilder


class SpringMassDamperSolver:
    def __init__(self, spring_mass_damper_d: SpringMassDamperAssembler, road_input: InputBuilder):

        self.__mass__ = spring_mass_damper_d.mass.weight
        self.__spring__ = spring_mass_damper_d.spring.stiffness
        self.__damper__ = spring_mass_damper_d.damper.damping
#        self.__road_input__ = road_input.road_input
        self._results = SpringMassDamper_R()

    def __create_model__(self):
        pass

    def vectorfield(self, w, t, p,u):
        """
        Defines the differential equations for the coupled spring-mass system.

        Arguments:
            w :  vector of the state variables:
                      w = [x1,x2]
            t :  time
            p :  vector of the parameters:
                      p = [m1,k1,c1]
        """
        x1, x2 = w
        m1, k1, c1 = p

        self.save_step()
        self._results.time = np.append(self._results.time, t)
        self._results.displacement = np.append(self._results.displacement, x1)
        self._results.velocity = np.append(self._results.velocity, x2)

        # Create f = (x1',y1',x2',y2'):
        f = [
            x2,
            (-k1/m1) * x1 - (c1/m1)*x2 + 1/m1 + u
             ]

        return f

    def save_step(self):
        self._results.weight = np.append(self._results.weight, self.__mass__)
        self._results.damping = np.append(self._results.weight, self.__mass__)
        self._results.stiffness = np.append(self._results.weight, self.__mass__)

    def get_results(self):
        return self._results

    def run_simulation(self):
        # ODE solver parameters
        abserr = 1.0e-8
        relerr = 1.0e-6
        stoptime = 50.0
        numpoints = 250

        # Create the time samples for the output of the ODE solver.
        # I use a large number of points, only because I want to make
        # a plot of the solution that looks nice.
        t = [stoptime * float(i) / (numpoints - 1) for i in range(numpoints)]

        x1 = 0
        x2 = 0

        m1 = self.__mass__
        k1 = self.__spring__
        c1 = self.__damper__

        # Pack up the parameters and initial conditions:
        p = [m1, k1, c1]
        w0 = [x1, x2]
        u = t
        # Call the ODE solver.
        wsol = odeint(self.vectorfield, w0, t, args=(p,5),
                      atol=abserr, rtol=relerr)

        return wsol