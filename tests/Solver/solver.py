from Design.quarter import SpringMassDamperAssembler
from Solver.spring_mass_damper import SpringMassDamperSolver

def test_solver():
    smd = SpringMassDamperAssembler()
    smd.initialize_default_values()

    model = SpringMassDamperSolver(smd, None)
    wsol = model.run_simulation()



    smd_R = model.get_results()


    smd_R.plot_result("Time","Displacement")
    smd_R.plot_result("Time","Velocity")

