from Design.quarter import SpringMassDamperAssembler
from Solver.smd_s import  SpringMassDamperSolver
import matplotlib.pyplot as plt

def test_design_values():
    smd = SpringMassDamperAssembler()
    smd.initialize_default_values()

    assert smd.damper.damping == 50
    assert smd.mass.weight == 100
    assert smd.spring.stiffness == 100