import numpy as np
import matplotlib.pyplot as plt


class SpringMassDamper_R:

    def __init__(self):
        self.weight = np.empty(0)
        self.damping = np.empty(0)
        self.stiffness = np.empty(0)
        self.time = np.empty(0)
        self.displacement = np.empty(0)
        self.velocity = np.empty(0)



    def plot_result(self, x_name, y_name):
        x = self.__get_value__(x_name)
        y = self.__get_value__(y_name)
        plt.plot(x, y)
        plt.show()


    def __get_value__(self, name):
        if name == "Time":
            return self.time
        elif name == "Displacement":
            return self.displacement
        elif name == "Velocity":
            return self.velocity